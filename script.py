# Utiliza a camada ativa no painel de camadas
camada = iface.activeLayer()

def transforma_src(geometria: QgsGeometry) -> QgsGeometry:
    """Faz a transformação do SRC do vetor para Albers 102033,
    permitindo calculo de área em Hectares"""
    geom = geometria
    crs_destino = QgsCoordinateReferenceSystem(102033)
    transformacao = QgsCoordinateTransform(camada.crs(), crs_destino, QgsProject.instance())
    geometria.transform(transformacao)
    return geom


# lista todos os campos da camada
campos = [campo.name() for campo in camada.fields()]

# Seleciona apenas os municipios mineiros
camada.selectByExpression(""""geocodigo" like '31%'""")
feicoes = camada.selectedFeatures()

# Montando tuplas com os campos "nome" e "Geocodigo"
dados = [(feicao[campos[1]], feicao[campos[4]]) for feicao in feicoes]

dado_area = [(feicao[campos[1]], feicao[campos[4]], transforma_src(feicao.geometry()).area()/10000) for feicao in feicoes]
