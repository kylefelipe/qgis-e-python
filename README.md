# Palestra Geoprocessamento com Qgis e Python

Palestra apresentada no 2º PyComunities Experiences - 01/2019

* [Apresentação](https://www.canva.com/design/DADw7E3TtEE/_NEaQLunCQAegnYA_WBTAA/view?utm_content=DADw7E3TtEE&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink);

* python_qgis.qgz - Arquivo de projeto do [Qgis](www.qgis.org);

* script.py - script utilizado no projeto;

* bcim_2016_21_11_2018.gpkg - arquivo geopackage com os dados do IBGE.

* [Script Standalone](https://gitlab.com/kylefelipe/palestra_cbgeo_2019)